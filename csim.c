/*
 *
 * Simulates operation of cache using Least Recently Used replacement policy
 * Takes in valgrind memory trace 
 * Performs loads, stores and modifications of values in cache
 * Tracks hit count, miss count and eviction count
 *
 * Works for direct mapped caches
 *
 * FALL 2015
 */
#include "cachelab.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>

typedef unsigned long long int mem_addr;

struct Line{
	int valid;
	mem_addr tag;
	int LRU;
};

typedef struct Line Line;

void load(Line* cache, int E, int setindex, mem_addr tag, int* hit, int* miss, int*evict);
void store(Line* cache, int E, int setindex, mem_addr tag, int* hit, int*miss, int* evict);
void modify(Line* cache, int E, int setindex, mem_addr tag, int* hit, int* miss, int*evict);
bool cacheHit(Line* cache, int E, int setindex, mem_addr tag);
bool setFull(Line* cache, int setindex, int E);
void updateLRU(Line* cache, int E,int setindex);
int maxLRU(Line* cache, int E,int setindex);


//Parses command lines. Initializes the cache. Runs the cache simulation on a memory trace.
int main(int argc, char *argv[]) {
	int hit_count = 0;
	int miss_count = 0;
	int eviction_count = 0;
	
	/**********************--------------------------***********************/
	/**********************     PARSE CMD LINE ARGS  ***********************/
	/**********************--------------------------***********************/
	int s = 0;
	int E = 0;
	int b = 0;
	char* filename = NULL; 
	FILE *infile;
	int c;
	
	opterr = 0;
	while ((c = getopt (argc, argv, "s:E:b:t:")) != -1)
	switch (c){
		case 's':
			s = atoi(optarg);
			break;
		case 'E':
			E = atoi(optarg);
			break;
		case 'b':
			b = atoi(optarg);
			break;
		case 't': 
			filename = optarg;
			break;
		case '?':
			if (optopt == 'c')
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint (optopt))
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		default:
			abort ();
	}
	
	infile = fopen(filename, "r");
	
	printf("s is %d E is %d b is %di\n", s, E, b);
	int index;
	for (index = optind; index < argc; index++)
		printf ("Non-option argument %s\n", argv[index]);
		
	/**********************--------------------------***********************/
	/**********************     INITITLIZE CACHE     ***********************/
	/**********************--------------------------***********************/
	int S = pow(2, s);//sets per cache
	Line* cache = calloc(E*S,sizeof(Line)); //make cache
	
	/*READ IN FILE*/
	if(infile == NULL){
		printf("Error: file open\n");
		exit(1);
	}
	
	/**********************--------------------------***********************/
	/**********************   RUN CACHE SIMULATION   ***********************/
	/**********************--------------------------***********************/
	char instruction = 0;
	mem_addr mem_address = 0;
	int bytes_accessed = 0;
	
	int setindex = 0;
	mem_addr tag = 0;
	
	while(fscanf(infile, " %c %qx,%d", &instruction, &mem_address, &bytes_accessed)==3){
		if(instruction == 'I'){
			continue;
		}
		
		setindex = (mem_address >> b) & (int)(pow(2, s) -1);
		tag = (mem_address >> (b+s));
		
		switch(instruction){
			case 'L':printf("\n \nL:\n"); load(cache,  E,  setindex,  tag, &hit_count, &miss_count, &eviction_count);break;
			case 'S':printf("\n\nS:\n"); store(cache,  E, setindex,  tag, &hit_count, &miss_count, &eviction_count); break;
			case 'M':printf("\n\nM:\n"); modify(cache,  E,  setindex,  tag, &hit_count, &miss_count, &eviction_count); break;
			default: printf("Error reading instruction");
			break;
		}
	}
	
	free(cache);
	fclose(infile);
	printSummary(hit_count,miss_count,eviction_count);
	return 0;
}


//Returns TRUE if the memory access was a cache hit. That is, if the data accessed is in the cache.
bool cacheHit(Line* cache, int E, int setindex, mem_addr tag){
	bool cache_hit = false;
	int j = E*setindex;	//start at beginning of set
	int end = E*setindex + E;
	
	Line target = (* (cache + j));
	mem_addr cache_tag = target.tag;
	
	//search all lines in set for match
	while(j< end){
		target=(*(cache + j));
		cache_tag = target.tag;
		
		//check next line if no match
		while((target.valid == 0 || cache_tag != tag) && (j< end)){
			target= (*(cache + j));
			cache_tag = target.tag;
			j++;
		}
		if(target.valid == 1 && cache_tag == tag){
			cache_hit = true;
			return cache_hit;
		}
	}
	
	return cache_hit;
}

//assuming full
//to prove not full, must find one line with valid = 0
bool setFull(Line* cache, int setindex, int E){
	bool isFull = true;
	int j;
	for(j = E*setindex; j< ((E*setindex + E)); j++){
		if(( *(cache + j)).valid == 0 ){
			return false;
		}
	}
	return isFull;
}


//Handles load instructions
void load(Line* cache, int E, int setindex, mem_addr tag, int* hit, int* miss, int*evict){
	printf("Loading set %d tag %llu... \n", setindex, tag);
	
	bool cache_hit = cacheHit(cache, E, setindex, tag);
	
	if(cache_hit){
		(*hit)++; printf("Hit\n");
		updateLRU(cache, E, setindex);
	}else{
		store(cache,  E, setindex,  tag, hit, miss, evict);
	}
}

//Handles store instructions
void store(Line* cache, int E,int setindex, mem_addr tag, int*hit, int*miss, int* evict){
	printf("Storing set %d tag %llu... \n", setindex, tag);

	/*CREATE CACHE LINE*/
	Line target;
	target.valid = 1;
	target.tag = tag;
	target.LRU = 0;
	/*CACHE LINE CREATED*/
	
	bool isFull = setFull(cache, setindex, E); 
	bool cache_hit = cacheHit(cache, E, setindex, tag);
	
	if(cache_hit && !isFull){
		(*hit)++; //now must store and update LRU values
		printf("Hit\n");
		int j;
		
		//search through set
		for(j = E*setindex; j< ((E*setindex + E)); j++){
			/*find match in set*/
			if(((*(cache + j)).valid ==1) && ( (*(cache + j)).tag == tag) ){
				//store new target with LRU = 0
				(*(cache + j)) = target;
				updateLRU(cache, E, setindex);
				break;
			}
		}
	}else if(cache_hit && isFull){
		(*hit)++; 
		printf("Hit\n");
		int j;
		for(j = E*setindex; j< ((E*setindex + E)); j++){
			//search for LRU: evict highest LRU
			if( (*(cache + j)).LRU== maxLRU(cache, E, setindex)){
				//add target line to that location in cache
				(*(cache + j)) = target;
				updateLRU(cache, E, setindex);
				break;
			}
		}
	}else if(!cache_hit && !isFull){
		(*miss)++;
		printf("Miss\n");
		int j;
		//search through set
		for(j = E*setindex; j< ((E*setindex + E)); j++){
		//if empty line in set
			if( (*(cache + j)).valid == 0 ){
                //store new value there
				(*(cache + j)) = target;
				updateLRU(cache, E, setindex);
				break;
			}
		}
	}else if(!cache_hit && isFull){
		(*miss)++;
		printf("Miss\n");
		int j;
		int end = ((E*setindex + E));
		for(j = E*setindex; j<=end-1 ; j++){
			//search for LRU - evict highest LRU
			if( (*(cache + j)).LRU== maxLRU(cache, E, setindex)){
				//add target line to that location in cache
				(*(cache + j)) = target;
				updateLRU(cache, E, setindex);
				(*evict)++;
				printf("Evict\n");
				break;
			}
		}
	}
}

//Handles modify instructions
void modify(Line* cache, int E, int setindex, mem_addr tag, int* hit, int* miss, int*evict){
	load(cache,  E,  setindex,  tag, hit, miss, evict); //read
	store(cache,  E, setindex,  tag, hit, miss, evict); //write
}

//Updates (increments) the LRU (least recently used) flag of each entry in the cache.
void updateLRU(Line* cache, int E,int setindex){
	int j;
	//search through set
	for(j = E*setindex; j< ((E*setindex + E)); j++){
		(*(cache + j)).LRU++; //evict highest one bc added LRU = 0
	}
}


//The item with the max LRU is the least recently used item in the cache.
//This item will be the first to be evicted when the cache reaches max capacity.
int maxLRU(Line* cache, int E,int setindex){
	int max_LRU = 0;
	int j;
	
	//search through set
	for(j = E*setindex; j< ((E*setindex + E)); j++){
		if(( (*(cache + j)).LRU) > max_LRU){
			max_LRU = (*(cache + j)).LRU;
		}
	}
	return max_LRU;
}



