# High Level Description:

For this project, I wrote a C program that simulates the behavior of a cache memory. The simulator reads in a parametrized description of the cache, reads in a trace file that specifies a number of memory accesses, simulates the hit/miss behavior of a cache memory on this trace, then outputs the total number of hits, misses, and evictions. The cache uses LRU (least-recently used) replacement policy when choosing which cache line to evict.

## Input: Parametrized description of the cache: 
The cache is described via command line by the following parameters 

* s = Number of set index bits 
* E = Associativity (number of lines per set) 
* b = Number of block bits

## Input: Trace file:
The trace files were generated using Valgrind. The trace contains a list of each of its memory accesses in the order that they occur and prints them on stdout. 
Valgrind memory traces have the following format per line: [operation address, size]

* operation = type of memory access
	* I = instruction load
	* L = data load
	* S = store
	* M - data modify
* address = hexadecimal memory address
* size = number of bytes accessed by the operation

## Output
The program runs and outputs the result (miss, hit, eviction) of each memory access as well as the total number of hits, misses and evictions for the given tracefile.